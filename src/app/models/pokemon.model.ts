export interface Pokemon {
    name: string;
    url: string;
    imgUrl: string;
    id:number;
}
