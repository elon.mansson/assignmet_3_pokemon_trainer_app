import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { FavouriteService } from 'src/app/services/favourite.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {

  public isFavourite: boolean = false;
  public listOfFavouritePokemons: number[] = [];
  @Input() pokemons: Pokemon[] = [];

  constructor(
    private readonly favouriteService: FavouriteService,
    private readonly userService: UserService
  ) { }


  //Update an array to update the DOM
  ngOnInit(): void {
    this.userService.user?.pokemon.map(pokemon => this.listOfFavouritePokemons.push(pokemon.id))
  }

  onFavouriteClick(pokemon: Pokemon): void {
    this.favouriteService.addToFavourite(pokemon)
    .subscribe({
      next: (response: User) => {
        //Dom reset if the favourite pokemon is allready in the array.
        if(this.listOfFavouritePokemons.includes(pokemon.id) === true){
          this.listOfFavouritePokemons = []
        }
        this.userService.user?.pokemon.map(pokemon => this.listOfFavouritePokemons.push(pokemon.id))
      },
      error: (error: HttpErrorResponse) => {
        console.log(error.message)
      }
    })
  }

}
