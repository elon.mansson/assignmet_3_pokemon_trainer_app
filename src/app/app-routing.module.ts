import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { LoginPage } from './pages/login/login.page';
import { PokemonPage } from './pages/pokemon/pokemon.page';
import { TrainerPage } from './pages/trainer/trainer.page';

const routes: Routes = [{
  path: "",
  pathMatch: "full",
  redirectTo: "/login"
},{
  path: "login",
  component: LoginPage,
},{
  path: "trainer",
  component: TrainerPage,
  canActivate: [ AuthGuard ]
},{
  path: "pokemon",
  component: PokemonPage,
  canActivate: [ AuthGuard ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
