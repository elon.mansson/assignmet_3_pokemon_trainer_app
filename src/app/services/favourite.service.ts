import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Input, OnInit } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { UserService } from './user.service';

const {apiKey, apiUsers} = environment

@Injectable({
  providedIn: 'root'
})
export class FavouriteService implements OnInit {

  private _loading: boolean = false;

  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonCatalogueService,
    private readonly userService: UserService,
  ) { }

  ngOnInit(): void {
  }

  public addToFavourite(pokemon: Pokemon): Observable<User>{
    if(!this.userService.user){
      throw new Error("There is no user")
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    })
    
    const user: User = this.userService.user;
    
    if(this.userService.inFavourites(pokemon.id)){
      this.userService.removeFromFavourites(pokemon.id)
    } else {
      this.userService.user.pokemon = [...user.pokemon, pokemon]
    }

    this._loading = true

    return this.http.patch<User>(`${apiUsers}/${user.id}`, {
      pokemon: [...user.pokemon]}, {
        headers
      }
    )
    .pipe(
      tap((updatedUser: User) => {
        this.userService.user = updatedUser
      }),
      finalize(() => {
        this._loading = false
      })
    )
  }
}
