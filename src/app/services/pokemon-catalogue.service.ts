import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';

const {apiPokemons} = environment

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  private _pokemons: Pokemon[] = []
  private _error: string = "";
  private _loading: boolean = false;

  get pokemons(): Pokemon[]{
    return this._pokemons;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  constructor(private readonly http: HttpClient) { }

  public findAllPokemons() : void {
    if(this._pokemons.length > 0 || this._loading){
      return;
    }

    this._loading = true;

    this.http.get<Pokemon[]>(apiPokemons)
    .pipe(
       map((x: any) => x.results)
      )
      .subscribe({
        next: (pokemons: Pokemon[]) => {
              this._pokemons = pokemons
              this._pokemons.map((pokemon, i) => {pokemon.id = i + 1; pokemon.imgUrl = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemon.id}.png`})
              this._loading = false
        },
        error: (error:HttpErrorResponse) => {
            this._error = error.message;
        }
      
      })
  } 
}
 /*  public pokemonByName(name: string): Pokemon | undefined {
    return this._pokemons.find((pokemon: Pokemon) => pokemon.results.name === id)
  } */
