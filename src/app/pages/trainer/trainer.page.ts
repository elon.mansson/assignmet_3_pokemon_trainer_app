import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { UserService } from 'src/app/services/user.service';
import { User } from '../../models/user.model'

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {

  get user(): User | undefined {
    return this.userService.user;
  }

  get favourites(): Pokemon[] {
    if(this.userService.user){
      return this.userService.user.pokemon;
    }
    return [];  
  }

  constructor(private readonly userService: UserService) { }

  ngOnInit(): void {
  }

}
