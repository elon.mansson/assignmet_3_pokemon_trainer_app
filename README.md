# Assignment 3, Pokemon Trainer App 

# Install
To run the project;
```
git clone https://gitlab.com/elon.mansson/assignmet_3_pokemon_trainer_app
cd assignmet_3_pokemon_trainer_app
npm install 
```
This will clone the repo to the folder and install the dependencies needed.

# Usage
To run the app, remember to have angular CLI and run the command:

```
ng serve
```

This will start the app on your localhost with port 4200.

To connect to the REST API, an environment file has to be created and placed in app/environments. Create files for environment.prod.ts and one environment.ts This file has to contain the following: 

```
  production: false/true (true in the .prod file),
  apiPokemons: 'https://pokeapi.co/api/v2/pokemon?limit=200&offset=0',
  apiUsers: <API_URL_HERE>,
  apiKey: <API_KEY_HERE>
```

# Contributors
[Jacob Andersson (@Jacob_A)](@Jacob_A)

[Elon Månsson (@elon.mansson)](@elon.mansson)

# Contributing
PRs are not accepted for this project.
